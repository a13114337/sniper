#include "simulator.h"
#include "cache.h"
#include "log.h"
#include "stdio.h"
#include <unistd.h>
#include "../../../misc/tracefile.h"

// Cache class
// constructors/destructors
/* Luo Modify start */
#ifdef L2_CACHE_LOG_ENABLE
void Cache::L2_Log_creator() {
   m_enable_log = false ;
   m_trace_file = NULL ;
   m_cache_hit_To_Log = false ;
   if ( m_Name.compare("L2")==0 ) {
     m_enable_log = true ;
	   String Path ( G_OutputPath->c_str()) ;
	   Path.append("/L2_Cache_trace_0.txt") ;
     m_trace_file = fopen( Path.c_str(),"w") ;
     fprintf( m_trace_file, "T\tOP\tADDR\tDATA\n" ) ;
	 printf( "\nTraceFile:%s\n\n", Path.c_str()) ;	 
   }  // if
}  // Cache::L2_Log_creator()

void Cache::L2_trace_File_mgr(){
  static int file_id = 1 ;
  static int Line = 0 ;
  if ( Line == 100000 ) {
    String Path ( G_OutputPath->c_str()) ;
    Path.append( "/L2_Cache_trace_" ) ;
    Path.append( std::to_string(file_id).c_str()) ;
    Path.append( ".txt" ) ;
    fclose( m_trace_file ) ;
    m_trace_file = fopen( Path.c_str(),"w") ;
    fprintf( m_trace_file, "T\tOP\tADDR\tDATA\n" ) ;
    Line = 0 ;
    file_id++ ;
  }  // if
  else Line ++ ; 
}  // Cache::L2_trace_File_mgr()

#endif
/* Luo Modify end */

Cache::Cache(
   String name,
   String cfgname,
   core_id_t core_id,
   UInt32 num_sets,
   UInt32 associativity,
   UInt32 cache_block_size,
   String replacement_policy,
   cache_t cache_type,
   hash_t hash,
   FaultInjector *fault_injector,
   AddressHomeLookup *ahl)
:
   CacheBase(name, num_sets, associativity, cache_block_size, hash, ahl),
   m_enabled(false),
   m_num_accesses(0),
   m_num_hits(0),
   m_cache_type(cache_type),
   m_fault_injector(fault_injector)
{
   m_Name = name ;
   m_set_info = CacheSet::createCacheSetInfo(name, cfgname, core_id, replacement_policy, m_associativity);
   m_sets = new CacheSet*[m_num_sets];
   for (UInt32 i = 0; i < m_num_sets; i++)
   {
      m_sets[i] = CacheSet::createCacheSet(cfgname, core_id, replacement_policy, m_cache_type, m_associativity, m_blocksize, m_set_info);
   }

   #ifdef ENABLE_SET_USAGE_HIST
   m_set_usage_hist = new UInt64[m_num_sets];
   for (UInt32 i = 0; i < m_num_sets; i++)
      m_set_usage_hist[i] = 0;
   #endif

/* Luo Modify start */
#ifdef L2_CACHE_LOG_ENABLE
   L2_Log_creator();
#endif
/* Luo Modify end */
}

Cache::~Cache()
{
   #ifdef ENABLE_SET_USAGE_HIST
   printf("Cache %s set usage:", m_Name.c_str());
   for (SInt32 i = 0; i < (SInt32) m_num_sets; i++)
      printf(" %" PRId64, m_set_usage_hist[i]);
   printf("\n");
   delete [] m_set_usage_hist;
   #endif

   if (m_set_info)
      delete m_set_info;

   for (SInt32 i = 0; i < (SInt32) m_num_sets; i++)
      delete m_sets[i];
   delete [] m_sets;
}

Lock&
Cache::getSetLock(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;

   splitAddress(addr, tag, set_index);
   assert(set_index < m_num_sets);

   return m_sets[set_index]->getLock();
}

bool
Cache::invalidateSingleLine(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;

   splitAddress(addr, tag, set_index);
   assert(set_index < m_num_sets);

   return m_sets[set_index]->invalidate(tag);
}

CacheBlockInfo*
Cache::accessSingleLine(IntPtr addr, access_t access_type,
      Byte* buff, UInt32 bytes, SubsecondTime now, bool update_replacement)
{
   //assert((buff == NULL) == (bytes == 0));

   IntPtr tag;
   UInt32 set_index;
   UInt32 line_index = -1;
   UInt32 block_offset;

   splitAddress(addr, tag, set_index, block_offset);

   CacheSet* set = m_sets[set_index];
   CacheBlockInfo* cache_block_info = set->find(tag, &line_index);

   if (cache_block_info == NULL)
      return NULL;
/* Luo Modify start */
#ifdef L2_CACHE_LOG_ENABLE
   if ( m_enable_log && m_cache_hit_To_Log ) 
     L2_trace_File_mgr();
#endif
/* Luo Modify end */  
   if (access_type == LOAD)
   {
      // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
      if (m_fault_injector)
         m_fault_injector->preRead(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);

      set->read_line(line_index, block_offset, buff, bytes, update_replacement);
	  
/* Luo Modify start */
#ifdef L2_CACHE_LOG_ENABLE
      if ( m_enable_log && m_cache_hit_To_Log  ) {    
        int * a = (int * ) addr ;
		int value = *a ;     
	    fprintf( m_trace_file, "%" PRId64 "\tR\t0x%016" PRIXPTR "\t%d", now.getNS(), addr, value ) ;
		fprintf( m_trace_file, "\n" ) ;
        fflush( m_trace_file );
        m_cache_hit_To_Log = false ;
      } // if
#endif
/* Luo Modify end */
   }
   else
   {
      if ( m_enable_log && m_cache_hit_To_Log  )
        printf("%s W\n", m_Name.c_str()) ;
      set->write_line(line_index, block_offset, buff, bytes, update_replacement);
/* Luo Modify start */
#ifdef L2_CACHE_LOG_ENABLE
      if ( m_enable_log && m_cache_hit_To_Log  ) {    
      
	      fprintf( m_trace_file, "%" PRId64 "\tW\t0x%016" PRIXPTR "\t", now.getNS(), addr ) ;
		    int * Daddr = 0;
		    uint64_t temp = 0;
        for ( int i = 0 ; i < 8 ;i++ ){
			    temp = temp << 8 ;
			    temp = temp + buff[block_offset+7-i] ;
		    }
		    Daddr = (int *)temp ;
		    fprintf(  m_trace_file, "%016X", *Daddr ) ;
        fprintf( m_trace_file, "\n" ) ;
        fflush( m_trace_file );
        m_cache_hit_To_Log = false ;
      } // if   
#endif
/* Luo Modify end */
      // NOTE: assumes error occurs in memory. If we want to model bus errors, insert the error into buff instead
      if (m_fault_injector)
         m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, bytes, (Byte*)m_sets[set_index]->getDataPtr(line_index, block_offset), now);
   }

   return cache_block_info;
}

void
Cache::insertSingleLine(IntPtr addr, Byte* fill_buff,
      bool* eviction, IntPtr* evict_addr,
      CacheBlockInfo* evict_block_info, Byte* evict_buff,
      SubsecondTime now, CacheCntlr *cntlr)
{
   IntPtr tag;
   UInt32 set_index;
   splitAddress(addr, tag, set_index);

   CacheBlockInfo* cache_block_info = CacheBlockInfo::create(m_cache_type);
   cache_block_info->setTag(tag);

   m_sets[set_index]->insert(cache_block_info, fill_buff,
         eviction, evict_block_info, evict_buff, cntlr);
   *evict_addr = tagToAddress(evict_block_info->getTag());

   if (m_fault_injector) {
      // NOTE: no callback is generated for read of evicted data
      UInt32 line_index = -1;
      __attribute__((unused)) CacheBlockInfo* res = m_sets[set_index]->find(tag, &line_index);
      LOG_ASSERT_ERROR(res != NULL, "Inserted line no longer there?");

      m_fault_injector->postWrite(addr, set_index * m_associativity + line_index, m_sets[set_index]->getBlockSize(), (Byte*)m_sets[set_index]->getDataPtr(line_index, 0), now);
   }

   #ifdef ENABLE_SET_USAGE_HIST
   ++m_set_usage_hist[set_index];
   #endif

   delete cache_block_info;
}


// Single line cache access at addr
CacheBlockInfo*
Cache::peekSingleLine(IntPtr addr)
{
   IntPtr tag;
   UInt32 set_index;
   splitAddress(addr, tag, set_index);

   return m_sets[set_index]->find(tag);
}

void
Cache::updateCounters(bool cache_hit)
{
   if (m_enabled)
   {
      m_num_accesses ++;
      if (cache_hit)
         m_num_hits ++;
   }
}

void
Cache::updateHits(Core::mem_op_t mem_op_type, UInt64 hits)
{
   if (m_enabled)
   {
      m_num_accesses += hits;
      m_num_hits += hits;
   }
}
